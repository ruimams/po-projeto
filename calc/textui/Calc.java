package calc.textui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pt.utl.ist.po.ui.Menu;
import static pt.utl.ist.po.ui.UserInteraction.IO;

import calc.core.Sheet;
import calc.core.Cell;
import calc.core.content.Content;
import calc.menu.CalcMenu;
import calc.textui.edit.InvalidCellRange;


/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Calc {
	
	public static void main(String args[]) {
		Sheet sheet;

		/* Read an Import file, if any */
		String filename = System.getProperty("import");
		if (filename != null) {
			sheet = parseInputFile(filename);
		}
		else {
			sheet = new Sheet();
		}

		IO.setTitle("Folha de Cálculo");
		Menu m = new CalcMenu(sheet);
		
		if (!sheet.isEmpty()) {
			m.entry(2).visible();
			m.entry(3).visible();
			m.entry(4).visible();
			m.entry(5).visible();
		}
		
		m.open();

		IO.close();
	}

	/**
	 * Parses an input file.
	 * 
	 * @param filename the name of the file to be parsed.
	 */
	public static Sheet parseInputFile(String filename) {
		Sheet sheet = null;
		int lineno = 0;

		try (BufferedReader in = new BufferedReader(new FileReader(filename))) { // see http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
			String s;
			int rows, columns;
			
			s = in.readLine();
			rows = Integer.parseInt(s.split("=")[1]);

			lineno++;
			s = in.readLine();
			columns = Integer.parseInt(s.split("=")[1]);

			sheet = new Sheet(rows, columns);

			lineno++;
			while ((s = in.readLine()) != null) {
				String line = new String(s.getBytes(), "UTF-8");
				
				try {
					parseLine(sheet, line);
				} catch (Exception e) {
					// File always well formated so no exception
				}

				lineno++;
			}

		} catch (FileNotFoundException e) {
			System.out.println("Erro (file not found):"+filename+": "+e);
		} catch (IOException e) {
			System.out.println("Erro (IO):"+filename+":"+lineno+": line "+e);
		}

		return sheet;
	}

	/**
	 * Parses a line of a file.
	 * 
	 * @param line the line of text to be parsed.
	 */
	public static void parseLine(Sheet sheet, String line) throws InvalidCellRange {
		int row, column;
		String expression;
		Cell cell;

		Pattern p = Pattern.compile("(\\d+);(\\d+)\\|(.*)");
		Matcher m = p.matcher(line);
		m.find();

		row = Integer.parseInt(m.group(1));
		column = Integer.parseInt(m.group(2));
		expression = m.group(3);

		cell = new Cell(row, column, expression, Content.parse(sheet, expression));
		sheet.setCell(row, column, cell);
	}
}