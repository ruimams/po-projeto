package calc.menu;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Menu;

import calc.core.Sheet;
import calc.textui.search.MenuEntry;
import calc.menu.search.*;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class CalcSearchMenu extends Menu {

	public CalcSearchMenu(Sheet sheet) {
		super(MenuEntry.TITLE, new Command<?>[] {
			new SearchValues(sheet),
			new SearchFunctions(sheet)
		});
	}
}