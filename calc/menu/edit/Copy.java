package calc.menu.edit;

import java.io.IOException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputString;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.edit.*;

import calc.core.Sheet;
import calc.core.Range;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Copy extends Command<Sheet> {
	
	public Copy(Sheet sheet) {
		super(MenuEntry.COPY, sheet);
	}


	@Override
	public final void execute() throws InvalidOperation {
		Form f = new Form(title());
		InputString inputAddress = new InputString(f, Message.addressRequest());
		f.parse();
		String address = inputAddress.value();

		try {
			entity().setCutBuffer(Range.parse(address));
		} catch (Exception e) {
			throw new InvalidCellRange(address);
		}
	}
}
