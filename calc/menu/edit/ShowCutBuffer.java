package calc.menu.edit;

import java.io.IOException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.edit.*;

import calc.core.Sheet;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class ShowCutBuffer extends Command<Sheet> {
	
	public ShowCutBuffer(Sheet sheet) {
		super(MenuEntry.SHOW_CUT_BUFFER, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		if (entity().getCutBuffer() != null) {
			(new Display(title())).add(entity().getCutBuffer().toString()).display();
		}
	}
}
