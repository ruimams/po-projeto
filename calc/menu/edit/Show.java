package calc.menu.edit;

import java.io.IOException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputString;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.edit.*;

import calc.core.Sheet;
import calc.core.Range;
import calc.core.CompositeGamma;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Show extends Command<Sheet> {
	
	public Show(Sheet sheet) {
		super(MenuEntry.SHOW, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		Form f = new Form(title());
		InputString address = new InputString(f, Message.addressRequest());
		f.parse();

		String parse = address.value();

		try {			
			(new Display(title())).add( new CompositeGamma(entity(), Range.parse(address.value()), true).toString() ).display();
		} catch (Exception e) {
			throw new InvalidCellRange(parse);
		}
	}
}
