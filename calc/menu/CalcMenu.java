package calc.menu;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Menu;
import pt.utl.ist.po.ui.Display;

import calc.textui.main.MenuEntry;
import calc.core.Sheet;
import calc.menu.main.*;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class CalcMenu extends Menu {

	public CalcMenu(Sheet sheet) {
		super(MenuEntry.TITLE, new Command<?>[] {
			new New(sheet),
			new Open(sheet),
			new Save(sheet),
			new SaveAs(sheet),
			new EditMenu(sheet),
			new SearchMenu(sheet)
		});

		entry(2).invisible();
		entry(3).invisible();
		entry(4).invisible();
		entry(5).invisible();
	}
}
