package calc.menu;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Menu;

import calc.core.Sheet;
import calc.core.Gamma;
import calc.textui.edit.MenuEntry;
import calc.menu.edit.*;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class CalcEditMenu extends Menu {
	
	public CalcEditMenu(Sheet sheet) {
		super(MenuEntry.TITLE, new Command<?>[] {
			new Show(sheet),
			new Insert(sheet),
			new Copy(sheet),
			new Delete(sheet),
			new Cut(sheet),
			new Paste(sheet),
			new ShowCutBuffer(sheet)
		});
	}
}
