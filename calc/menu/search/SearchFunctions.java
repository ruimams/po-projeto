package calc.menu.search;

import java.io.IOException;
import java.util.List;
import java.lang.StringBuilder;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputString;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.search.*;

import calc.core.Sheet;
import calc.core.Cell;
import calc.core.CompositeGamma;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class SearchFunctions extends Command<Sheet> {
	
	public SearchFunctions(Sheet sheet) {
		super(MenuEntry.SEARCH_FUNCTIONS, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		Form f = new Form(title());
		InputString inputFunction = new InputString(f, Message.searchFunction());
		f.parse();

		CompositeGamma found = entity().searchFunction(inputFunction.value());
		
		if (!found.isEmpty()) {
			(new Display(title())).add(found.toString()).display();
		}
	}
}
