package calc.menu.main;

import java.io.IOException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputString;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.main.*;

import calc.core.Sheet;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class SaveAs extends Command<Sheet> {
	
	public SaveAs(Sheet sheet) {
		super(MenuEntry.SAVE_AS, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		Form f = new Form(title());
		InputString name = new InputString(f, Message.saveAs());
		f.parse();
		entity().setName(name.value());

		new Save(entity()).saveFile();
	}
}
