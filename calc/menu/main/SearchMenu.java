package calc.menu.main;

import java.io.IOException;

import pt.utl.ist.po.ui.Menu;
import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputInteger;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.core.Sheet;
import calc.menu.CalcSearchMenu;
import calc.textui.main.MenuEntry;
import calc.textui.main.Message;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class SearchMenu extends Command<Sheet> {
	
	public SearchMenu(Sheet sheet) {
		super(MenuEntry.MENU_SEARCH, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		Menu m = new CalcSearchMenu(entity());
		m.open();
	}
}