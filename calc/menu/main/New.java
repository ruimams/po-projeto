package calc.menu.main;

import java.io.IOException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputBoolean;
import pt.utl.ist.po.ui.InputInteger;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.main.*;

import calc.core.Sheet;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class New extends Command<Sheet> {
	
	public New(Sheet sheet) {
		super(MenuEntry.NEW, sheet);
	}


	@Override
	public final void execute() throws InvalidOperation {
		Form f;

		if (!entity().isEmpty() && entity().isChanged()) {
			f = new Form(title());
			InputBoolean response = new InputBoolean(f, Message.saveBeforeExit());
			f.parse();

			if (response.value()) {
				new Save(entity()).execute();
			}
		}

		f = new Form(title());
		InputInteger lines = new InputInteger(f, Message.linesRequest());
		InputInteger columns = new InputInteger(f, Message.columnsRequest());
		f.parse();

		entity().setSheet(lines.value(), columns.value(), "");

		menu().entry(2).visible();
		menu().entry(3).visible();
		menu().entry(4).visible();
		menu().entry(5).visible();
	}
}
