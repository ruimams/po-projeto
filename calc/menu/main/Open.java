package calc.menu.main;

import java.io.IOException;
import java.io.FileNotFoundException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Menu;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputString;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.core.Sheet;
import calc.textui.main.MenuEntry;
import calc.textui.main.Message;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Open extends Command<Sheet> {
	
	public Open(Sheet sheet) {
		super(MenuEntry.OPEN, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		Form f = new Form(title());
		InputString file = new InputString(f, Message.openFile());
		f.parse();

		try {
			entity().load(file.value());
		} catch (FileNotFoundException e) {
			(new Display(title())).add(Message.fileNotFound(file.value())).display();
		} catch (Exception e) {
			System.out.println("Erro : "+file+" : "+e);
		}

		menu().entry(2).visible();
		menu().entry(3).visible();
		menu().entry(4).visible();
		menu().entry(5).visible();
	}
}
