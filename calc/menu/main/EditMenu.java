package calc.menu.main;

import java.io.IOException;

import pt.utl.ist.po.ui.Menu;
import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;

import pt.utl.ist.po.ui.InvalidOperation;

import calc.core.Sheet;
import calc.menu.CalcEditMenu;
import calc.textui.main.MenuEntry;
import calc.textui.edit.Message;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class EditMenu extends Command<Sheet> {
	
	public EditMenu(Sheet sheet) {
		super(MenuEntry.MENU_CALC, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		Menu m = new CalcEditMenu(entity());
		m.open();
	}
}