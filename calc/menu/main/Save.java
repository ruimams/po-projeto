package calc.menu.main;

import java.io.IOException;

import pt.utl.ist.po.ui.Command;
import pt.utl.ist.po.ui.Display;
import pt.utl.ist.po.ui.Form;
import pt.utl.ist.po.ui.InputString;
import pt.utl.ist.po.ui.InvalidOperation;

import calc.textui.main.*;

import calc.core.Sheet;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Save extends Command<Sheet> {
	
	public Save(Sheet sheet) {
		super(MenuEntry.SAVE, sheet);
	}

	
	@Override
	public final void execute() throws InvalidOperation {
		if (entity().isChanged()) {
			if (entity().getName().isEmpty()) {
				Form f = new Form(title());
				InputString name = new InputString(f, Message.newSaveAs());
				f.parse();
				entity().setName(name.value());
			}

			saveFile();
		}
	}

	public void saveFile() {
		try {
			entity().save();
		}
		catch (IOException e) {
			System.out.println("Erro : "+entity().getName()+" : "+e);
		}
	}
}
