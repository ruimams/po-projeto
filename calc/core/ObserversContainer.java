package calc.core;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

import calc.core.Observer;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class ObserversContainer implements Serializable {

	private int lines;
	private int columns;
	private List<List<List<Observer>>> observers;

	public ObserversContainer(int lines, int columns) {
		this.lines = lines;
		this.columns = columns;
		this.observers = new ArrayList<List<List<Observer>>>(lines);

		int l, c;
		for (l = 0; l < lines; l++) {
			this.observers.add(l, new ArrayList<List<Observer>>(columns));	
			for (c = 0; c < columns; c++) {
				this.observers.get(l).add(c, new ArrayList<Observer>());
			}
		}
	}

	public void add(int l, int c, Observer obs) {
		this.observers.get(l-1).get(c-1).add(obs);
	}

	public void remove(Observer obs) {
		int nPresence, found, l, c;
		nPresence = obs.getNumObservers();
		found = 0;

		for (l = 0; l < this.lines && found < nPresence; l++) {
			for (c = 0; c < this.columns && found < nPresence; c++) {
				while (this.observers.get(l).get(c).contains(obs)) {
					this.observers.get(l).get(c).remove(obs);
					found++;
				}
			}
		}
	}

	public void notifyObservers(int l, int c) {
		for (Observer obs : this.observers.get(l-1).get(c-1)) {
			obs.update();
		}
	}
}