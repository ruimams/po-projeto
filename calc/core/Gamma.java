package calc.core;

import java.io.Serializable;
import java.util.List;

import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public interface Gamma extends Serializable {

	public String toString();

	public List<Cell> getCells();

}