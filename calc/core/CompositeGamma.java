package calc.core;

import java.util.List;
import java.util.ArrayList;

import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class CompositeGamma implements Gamma {

	private Sheet sheet;
	private List<Gamma> gammas;

	public CompositeGamma(Sheet sheet) {
		this.sheet = sheet;
		this.gammas = new ArrayList<Gamma>();
	}

	public CompositeGamma(Sheet sheet, Range range, boolean fill) {
		this(sheet);
		int i, j;
		Cell cell;

		for (i = range.getStartingLine(); i <= range.getEndingLine(); i++) {
			for (j = range.getStartingColumn(); j <= range.getEndingColumn(); j++) {
				cell = sheet.getCell(i, j);

				if (cell == null && fill) {
					cell = new Cell(i, j);
				}

				this.gammas.add(cell);
			}
		}
	}

	public boolean isEmpty() {
		return this.gammas.isEmpty();
	}

	public void add(Gamma gamma) {
		this.gammas.add(gamma);
	}

	public Gamma get(int index) {
		return this.gammas.get(index);
	}

	public List<Cell> getCells() {
		List<Cell> cells = new ArrayList<Cell>();

		for (Gamma g : this.gammas) {
			try {
				cells.addAll(g.getCells());
			}
			catch (Exception e) {
				cells.add(null);
			}
		}

		return cells;
	}

	public String toString() {
		if (!isEmpty()) {
			StringBuilder text = new StringBuilder();

			for (Gamma g : this.gammas) {
				text.append(g.toString());
				text.append("\n");
			}
			text.deleteCharAt(text.length() - 1); // removing last newline

			return text.toString();
		}
		else {
			return "";
		}
	}

}