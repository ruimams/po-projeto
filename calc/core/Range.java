package calc.core;

import java.util.regex.PatternSyntaxException;
import java.lang.NumberFormatException;

import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Range {

	private final int startingLine;
	private final int startingColumn;
	private final int endingLine;
	private final int endingColumn;

	public Range(int startingLine, int startingColumn, int endingLine, int endingColumn) {
		this.startingLine = startingLine;
		this.startingColumn = startingColumn;
		this.endingLine = endingLine;
		this.endingColumn = endingColumn;
	}

	public int getStartingLine() {
		return this.startingLine;
	}

	public int getStartingColumn() {
		return this.startingColumn;
	}

	public int getEndingLine() {
		return this.endingLine;
	}

	public int getEndingColumn() {
		return this.endingColumn;
	}

	public int size() {
		return (this.endingLine - this.startingLine + 1) * (this.endingColumn - this.startingColumn + 1);
	}

	public static Range parse(String expression) throws InvalidCellRange {
		int startingLine, startingColumn, endingLine, endingColumn;
		
		if (expression.contains(":")) {
			String address1, address2;

			try {
				String[] address = expression.split(":");
				address1 = address[0];
				address2 = address[1];
			} catch (PatternSyntaxException e) {
				throw new InvalidCellRange(expression);
			}

			try {
				startingLine = Integer.parseInt(address1.split(";")[0]);
				startingColumn = Integer.parseInt(address1.split(";")[1]);
			} catch (PatternSyntaxException | NumberFormatException e) { // see http://www.oracle.com/technetwork/articles/java/java7exceptions-486908.html
				throw new InvalidCellRange(address1);
			}

			try {
				endingLine = Integer.parseInt(address2.split(";")[0]);
				endingColumn = Integer.parseInt(address2.split(";")[1]);
			} catch (PatternSyntaxException | NumberFormatException e) { // see http://www.oracle.com/technetwork/articles/java/java7exceptions-486908.html
				throw new InvalidCellRange(address2);
			}


			if (startingLine != endingLine && startingColumn != endingColumn) {
				throw new InvalidCellRange(expression);
			}
		}
		else {
			try {
				String[] address = expression.split(";");
				
				startingLine = Integer.parseInt(address[0]);
				endingLine = startingLine;
				startingColumn = Integer.parseInt(address[1]);
				endingColumn = startingColumn;
			} catch (PatternSyntaxException | NumberFormatException e) { // see http://www.oracle.com/technetwork/articles/java/java7exceptions-486908.html
				throw new InvalidCellRange(expression);
			}
		}

		return new Range(startingLine, startingColumn, endingLine, endingColumn);
	}
}