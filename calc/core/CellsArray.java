package calc.core;

import java.util.List;
import java.util.ArrayList;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class CellsArray extends CellContainer {
	private Cell[][] cells;
	private int rows;
	private int columns;
	
	/**
	* Creates a two-dimensional array with the size given and fills it with empty cells.
	*
 	* @param rows number of rows the matrix has
 	* @param columns number of columns the matrix has
	*/
	public CellsArray(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		this.cells = new Cell[rows][columns];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				this.cells[i][j] = new Cell(i+1, j+1);
			}
		}
	}

	public Cell getCell(int r, int c) {
		return this.cells[r-1][c-1];
	}

	public void setCell(int r, int c, Cell cell) {
		this.cells[r-1][c-1] = cell;
	}

	public List<Cell> getAllCells() {
		List<Cell> list = new ArrayList<Cell>();

		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.columns; j++) {
				list.add(this.cells[i][j]);
			}
		}

		return list;
	}
}