package calc.core;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public abstract class CellContainer implements Serializable {
	 /**
	 * Gets the cell with the given line and column.
	 *
	 * @param the line end the colunm of the cell.
	 */
	public abstract Cell getCell(int l, int c);
	 /**
	 * Puts a cell in the given line and column . 
	 *
	 * @param the line, column and a cell.
	 */
	public abstract void setCell(int l, int c, Cell cell);
	
	public abstract List<Cell> getAllCells();
}