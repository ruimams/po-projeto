package calc.core;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public interface Observer {
	
	public void	update();

	public boolean isObserving();

	public int getNumObservers();

	public void setNumObservers(int num);

	public void incrementObservers();

}