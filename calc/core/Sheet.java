package calc.core;

import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import calc.core.content.Content;
import calc.core.content.IndirectContent;

import calc.textui.edit.InvalidCellRange;


/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Sheet implements Serializable {
	/**
	 * The number of rows the sheet has.
	 */
	private int lines;

	/**
	 * The number of columns the sheet has.
	 */
	private int columns;

	/**
	 * The name of the sheet (needed to save to file).
	 */
	private String name;

	/**
	 * The cells this sheet has.
	 */
	private CellContainer cells;

	/**
	 * Represents if the sheet was changed after being loaded or saved for the last time.
	 */
	private boolean changed;


	private CompositeGamma cutBuffer;

	private ObserversContainer observers;

	/**
	 * Constructor. Creates a sheet.
	 *
	 * @param rows the number of rows the sheet has.
	 * @param columns the number of columns the sheet has.
	 */
	public Sheet(int rows, int columns) {
		this.lines = rows;
		this.columns = columns;
		this.name = "";
		this.cells = new SparseMatrix(rows, columns);
		this.changed = true;
		this.cutBuffer = null;
		this.observers = new ObserversContainer(rows, columns);
	}

	/**
	 * Constructor. Creates an empty sheet.
	 */
	public Sheet() {
		this(0, 0);
	}

	/**
	 * Sets sheet's propertys. Also sets the size of the sheet and creates a new list of cells with the requested size.
	 *
	 * @param lines the number of rows.
	 * @param columns the number of columns.
	 * @param name name of the sheet.
	 */
	public void setSheet(int lines, int columns, String name) {
		setSize(lines, columns);
		this.name = name;
	}

	/**
	 * Sets the size of the sheet and creates a new list of cells with the requested size.
	 * 
	 * @param lines the number of rows.
	 * @param columns the number of columns.
	 */
	public void setSize(int lines, int columns) {
		this.lines = lines;
		this.columns = columns;
		this.cells = new SparseMatrix(lines, columns);
		this.changed = true;
		this.cutBuffer = null;
		this.observers = new ObserversContainer(lines, columns);
	}

	/**
	 * Returns the cell positioned on the given coordinates.
	 * 
	 * @param line the line of the cell to be returned.
	 * @param column the column of the cell to be returned.
	 * 
	 * @return The cell positioned on the given coordinates or null if there is none.
	 */
	public Cell getCell(int line, int column) {
		return this.cells.getCell(line, column);
	}

	public void setCell(int line, int column, Cell cell) {
		Cell current = getCell(line, column);
		
		if (current != null) {
			Content currentContent = current.getContent();
			if (currentContent != null) {
				if (currentContent instanceof IndirectContent) {
					if (((IndirectContent) currentContent).isObserving()) {
						unregisterObserver(((Observer) currentContent));
					}
				}
			}
		}

		this.observers.notifyObservers(line, column);

		this.cells.setCell(line, column, cell);
	}

	public void setCell(Cell cell) {
		setCell(cell.getLine(), cell.getColumn(), cell);
	}

	/**
	 * Returns the name of the sheet.
	 * 
	 * @return the name of the sheet.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of the sheet.
	 * 
	 * @param name the new name of the sheet.
	 */
	public void setName(String name) {
		this.name = name;
	}

	public CompositeGamma getCutBuffer() {
		return this.cutBuffer;
	}

	public void setCutBuffer(Range range) {
		this.cutBuffer = new CompositeGamma(this);
		List<Cell> cells = new CompositeGamma(this, range, true).getCells();

		if (!cells.isEmpty()) {
			int deltaLine = cells.get(0).getLine() - 1;
			int deltaColumn = cells.get(0).getColumn() - 1;

			for (Cell c : cells) {
				this.cutBuffer.add(new Cell(c.getLine()-deltaLine, c.getColumn()-deltaColumn, c)); // deep copy cells and sets first cut buffer cell at 1;1
			}
		}
	}

	/**
	 * Returns if the sheet is empty (or not).
	 * 
	 * @return true if the sheet is empty false otherwise.
	 */
	public boolean isEmpty() {
		return ((this.lines * this.columns) == 0);
	}

	/**
	 * Returns if the sheet has changed after being created or saved (or not).
	 * 
	 * @return true if the sheet has changed false otherwise.
	 */
	public boolean isChanged() {
		return this.changed;
	}

	/**
	 * Defines that the sheet has changed.
	 * 
	 */
	public void change() {
		this.changed = true;
	}

	public CompositeGamma searchValue(int value) {
		CompositeGamma found = new CompositeGamma(this);
		
		for (Cell c : this.cells.getAllCells()) {
			if (!c.isEmpty()) {
				if (c.solve() == value) {
					found.add(c);
				}
			}
		}

		return found;
	}

	public CompositeGamma searchFunction(String function) {
		CompositeGamma found = new CompositeGamma(this);
		function = "=" + function;

		for (Cell c : this.cells.getAllCells()) {
			if (c.getExpression().startsWith(function)) {
				found.add(c);
			}
		}

		return found;
	}

	public void paste(Range range) {
		if (this.cutBuffer != null) {
			List<Cell> bufferList = this.cutBuffer.getCells();
			List<Cell> destList = new CompositeGamma(this, range, true).getCells();

			if (bufferList.size() == destList.size()) {
				Cell dest;

				for (int i = 0; i < bufferList.size(); i++) {
					dest = destList.get(i);

					setCell(new Cell(dest.getLine(), dest.getColumn(), bufferList.get(i)));
				}
			}
			else if (destList.size() == 1) { // bufferList.size() > 1
				int i, j = 0;

				if (bufferList.get(0).getLine() == bufferList.get(1).getLine()) { // horizontal
					for (i = destList.get(0).getColumn(); i <= this.columns; i++) {

						setCell(new Cell(destList.get(0).getLine(), i, bufferList.get(j % bufferList.size())));
						j++;
					}
				}
				else { // vertical
					for (i = destList.get(0).getLine(); i <= this.lines; i++) {

						setCell(new Cell(i, destList.get(0).getColumn(), bufferList.get(j % bufferList.size())));
						j++;
					}
				}
			}
		}
	}

	public void delete(Range range) {
		int l, c;

		for (l = range.getStartingLine(); l <= range.getEndingLine(); l++) {
			for (c = range.getStartingColumn(); c <= range.getEndingColumn(); c++) {
				setCell(l, c, null);
			}
		}
	}

	public void insert(Range range, String expression) throws InvalidCellRange {
		int l, c;

		for (l = range.getStartingLine(); l <= range.getEndingLine(); l++) {
			for (c = range.getStartingColumn(); c <= range.getEndingColumn(); c++) {
				setCell(l, c, new Cell(l, c, expression, Content.parse(this, expression)));
			}
		}
	}

	public void registerObserver(int line, int column, Observer obs) {
		this.observers.add(line, column, obs);
	}

	public void registerObserver(Range range, Observer obs) {
		int l, c;
		
		for (l = range.getStartingLine(); l <= range.getEndingLine(); l++) {
			for (c = range.getStartingColumn(); c <= range.getStartingColumn(); c++) {
				registerObserver(l, c, obs);
			}
		}
	}

	public void unregisterObserver(Observer obs) {
		this.observers.remove(obs);
	}

	/**
	 * Loads the previous state of the application that was saved on a file to this instance.
	 * 
	 * @param file the name of the file with the serialized data.
	 * 
	 * @throws IOException if there's any error while reading the state.
	 */
	public void load(String file) throws IOException, ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));

		Sheet sheet = (Sheet)in.readObject();
		in.close();

		this.lines = sheet.lines;
		this.columns = sheet.columns;
		this.name = file;
		this.cells = sheet.cells;
		this.changed = false;
	}

	/**
	 * Saves the current state of the application to a file.
	 * 
	 * @throws IOException if there's any error while saving the state.
	 */
	public void save() throws IOException {
		this.changed = false;

		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(this.name));
		
		out.writeObject(this);
		out.close();
	}
}