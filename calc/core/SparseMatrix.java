package calc.core;

import java.util.List;
import java.util.ArrayList;

public class SparseMatrix extends CellContainer {
	private Cell[][] cells;
	private int rows;
	private int columns;
	
	/**
	* Creates a two-dimensional array with the size given.
	*
 	* @param rows number of rows the matrix has
 	* @param columns number of columns the matrix has
	*/
	public SparseMatrix(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		this.cells = new Cell[rows][columns];
	}

	public Cell getCell(int r, int c) {
		return this.cells[r-1][c-1];
	}

	public void setCell(int r, int c, Cell cell) {
		this.cells[r-1][c-1] = cell;
	}

	public List<Cell> getAllCells() {
		List<Cell> list = new ArrayList<Cell>();

		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.columns; j++) {
				if (this.cells[i][j] != null) {
					list.add(this.cells[i][j]);
				}
			}
		}

		return list;
	}
}