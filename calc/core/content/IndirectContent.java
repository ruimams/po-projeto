package calc.core.content;

import calc.core.Observer;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public abstract class IndirectContent extends Content implements Observer {

	private Integer cachedSolve = null;

	private String cachedString = null;

	private int numObservers = 0;

	public Integer getCachedSolve() {
		return this.cachedSolve;
	}

	public void setCachedSolve(int newCache) {
		this.cachedSolve = newCache;
	}

	public String getCachedString() {
		return this.cachedString;
	}

	public void setCachedString(String newCache) {
		this.cachedString = newCache;
	}

	public void update() {
		this.cachedSolve = null;
		this.cachedString = null;
	}

	public boolean isObserving() {
		return this.numObservers > 0;
	}

	public int getNumObservers() {
		return this.numObservers;
	}

	public void setNumObservers(int num) {
		this.numObservers = num;
	}

	public void incrementObservers() {
		this.numObservers = this.numObservers + 1;
	}
}