package calc.core.content;

import calc.core.Sheet;
import calc.core.Cell;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Reference extends IndirectContent implements FunctionArgument {
	private Sheet sheet;

	private int row;

	private int column;

	public Reference(Sheet sheet, int row, int column) {
		this.sheet = sheet;
		this.row = row;
		this.column = column;
	}

	public int getLine() {
		return this.row;
	}

	public int getColumn() {
		return this.column;
	}

	@Override
	public int solve() {
		Integer cached = getCachedSolve();

		if (cached == null) {
			cached = this.sheet.getCell(this.row, this.column).getContent().solve();
			setCachedSolve(cached);
		}

		return cached;
	}

	@Override
	public String toString() {
		String cached = getCachedString();

		if (cached == null) {
			Cell c = this.sheet.getCell(this.row, this.column);
		
			if (c != null) {
				if (c.getContent() != null) {
					cached = c.getContent().toString();
				}
				else {
					cached = "#VALUE";
				}
			}
			else {
				cached = "#VALUE";
			}

			setCachedString(cached);
		}

		return cached;
	}

	public static Reference parse(Sheet sheet, String expression, boolean register) {
		String[] args = expression.split(";");
		int line = Integer.parseInt(args[0]);
		int column = Integer.parseInt(args[1]);

		Reference ref = new Reference(sheet, line, column);
		
		if (register) {
			sheet.registerObserver(line, column, ref);
			ref.incrementObservers();
		}

		return ref;
	}
}