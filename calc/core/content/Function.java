package calc.core.content;

import calc.core.Sheet;
import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public abstract class Function extends IndirectContent {

	public static Function parse(Sheet sheet, String expression) throws InvalidCellRange {
		String functionName = expression.split("\\(")[0];
		String args_exp = expression.substring(functionName.length()+1, expression.length()-1);
		
		if (expression.contains(",")) {
			return BinaryFunction.parse(sheet, functionName, args_exp);
		}
		else {
			return RangeFunction.parse(sheet, functionName, args_exp);
		}
	}
}