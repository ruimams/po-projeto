package calc.core.content;

import java.util.List;

import calc.core.Cell;
import calc.core.CompositeGamma;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Prd extends RangeFunction {

	public Prd(CompositeGamma cellRange) {
		super(cellRange);
	}
	
	@Override
	public int solve() {
		Integer cached = getCachedSolve();

		if (cached == null) {
			int prd = 1;
			List<Cell> cells = getCellRange().getCells();
			
			for (Cell c : cells) {
				prd *= c.getContent().solve();
			}

			cached = prd;
			setCachedSolve(cached);
		}

		return cached;
	}	
}