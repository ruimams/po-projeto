package calc.core.content;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Mul extends BinaryFunction {
	
	public Mul(FunctionArgument arg1, FunctionArgument arg2) {
		super(arg1, arg2);
	}

	@Override
	public int solve() {
		Integer cached = getCachedSolve();

		if (cached == null) {
			cached = getArg1().solve() * getArg2().solve();
			setCachedSolve(cached);
		}

		return cached;
	}
}