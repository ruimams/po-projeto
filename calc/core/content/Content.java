package calc.core.content;

import java.io.Serializable;

import calc.core.Sheet;
import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public abstract class Content implements Serializable {
	
	public abstract int solve();

	public abstract String toString();

	public static Content parse(Sheet sheet, String expression) throws InvalidCellRange {
		if (!expression.isEmpty()) {
			if (expression.startsWith("=")) { // Function or Reference
				String parse = expression.substring(1); 

				if (Character.isDigit(parse.charAt(0))) {
					return Reference.parse(sheet, parse, true);
				}
				else {
					return Function.parse(sheet, parse);
				}
			}
			else {
				return Literal.parse(expression);
			}
		}
		else { // empty expression
			return null;
		}
	}

	public static FunctionArgument parseLiteralOrReference(Sheet sheet, String expression) {
		if (expression.contains(";"))
			return Reference.parse(sheet, expression, false);
		else
			return Literal.parse(expression);
	}
}