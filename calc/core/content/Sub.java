package calc.core.content;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Sub extends BinaryFunction {
	/**
	 *
	 * @param 
	 */
	public Sub(FunctionArgument arg1, FunctionArgument arg2) {
		super(arg1, arg2);
	}
	/**
	 * @return the subtraction of the cell contents.
	 */
	@Override
	public int solve() {
		Integer cached = getCachedSolve();

		if (cached == null) {
			cached = getArg1().solve() - getArg2().solve();
			setCachedSolve(cached);
		}

		return cached;
	}
}