package calc.core.content;

import java.util.List;

import calc.core.Cell;
import calc.core.CompositeGamma;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Avg extends RangeFunction {
	/**
	 * Calculates the average of the contents.
	 * 
	 * @param a cell range.
	 * 
	 * @return the average of the contents .
	 */
	public Avg(CompositeGamma cellRange) {
		super(cellRange);
	}
	
	@Override
	public int solve() {
		Integer cached = getCachedSolve();

		if (cached == null) {
			int sum = 0;
			List<Cell> cells = getCellRange().getCells();

			for (Cell c : cells) {
				sum += c.getContent().solve();
			}

			cached = sum / cells.size();
			setCachedSolve(cached);
		}

		return cached;
	}
	
}
	