package calc.core.content;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Literal extends DirectContent implements FunctionArgument {
	private int value;

	public Literal(int value) {
		this.value = value;
	}

	@Override
	public int solve() {
		return this.value;
	}

	@Override
	public String toString() {
		return "" + this.value;
	}

	public static Literal parse(String expression) {
		return new Literal(Integer.parseInt(expression));
	}
}