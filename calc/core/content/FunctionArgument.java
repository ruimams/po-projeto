package calc.core.content;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public interface FunctionArgument {
	public int solve();
}