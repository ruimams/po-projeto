package calc.core.content;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Add extends BinaryFunction {
	/**
	 * 
	 */
	public Add(FunctionArgument arg1, FunctionArgument arg2) {
		super(arg1, arg2);
	}

	/**
	 * @return the sum of the contents.
	 */
	@Override
	public int solve() {
		Integer cached = getCachedSolve();

		if (cached == null) {
			cached = getArg1().solve() + getArg2().solve();
			setCachedSolve(cached);
		}

		return cached;
	}
}