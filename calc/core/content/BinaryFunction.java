package calc.core.content;

import calc.core.Sheet;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public abstract class BinaryFunction extends Function {
	
	private FunctionArgument arg1;

	private FunctionArgument arg2;
	
	/**
 	 * @param two arguments, one from each cell.
	 */
	public BinaryFunction(FunctionArgument arg1, FunctionArgument arg2) {
		this.arg1 = arg1;
		this.arg2 = arg2;
	}

	public FunctionArgument getArg1() {
		return this.arg1;
	}

	public FunctionArgument getArg2() {
		return this.arg2;
	}

	@Override
	public String toString() {
		String cached = getCachedString();

		if (cached == null) {
			if (this.arg1.toString() == "#VALUE" || this.arg2.toString() == "#VALUE") {
				cached = "#VALUE";
			}
			else {
				cached = "" + solve();
			}

			setCachedString(cached);
		}

		return cached;
	}
	
	/**
	 * 
 	 * @param 
 	 * @param 
 	 * @param 
	 */
	public static BinaryFunction parse(Sheet sheet, String functionName, String args_exp) {
		String[] args = args_exp.split(",");

		FunctionArgument arg1 = Content.parseLiteralOrReference(sheet, args[0]);
		FunctionArgument arg2 = Content.parseLiteralOrReference(sheet, args[1]);

		BinaryFunction binFn = null;
		
		switch (functionName) {
			case "ADD":
				binFn = new Add(arg1, arg2);
				break;
			case "SUB":
				binFn = new Sub(arg1, arg2);
				break;
			case "MUL":
				binFn = new Mul(arg1, arg2);
				break;
			case "DIV":
				binFn = new Div(arg1, arg2);
				break;
		}

		if (binFn != null) {
			Reference ref;
			if (arg1 instanceof Reference) {
				ref = (Reference) arg1;
				sheet.registerObserver(ref.getLine(), ref.getColumn(), binFn);
				binFn.incrementObservers();
			}
			if (arg2 instanceof Reference) {
				ref = (Reference) arg1;
				sheet.registerObserver(ref.getLine(), ref.getColumn(), binFn);
				binFn.incrementObservers();
			}
		}

		return binFn;
	}
}