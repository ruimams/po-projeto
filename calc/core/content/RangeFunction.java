package calc.core.content;

import java.util.List;

import calc.core.Sheet;
import calc.core.Cell;
import calc.core.CompositeGamma;
import calc.core.Range;
import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public abstract class RangeFunction extends Function {

	private CompositeGamma cellRange;
		
	public RangeFunction(CompositeGamma cellRange) {
		this.cellRange = cellRange;
	}
	
	public CompositeGamma getCellRange() {
		return this.cellRange;
	}
	
	@Override
	public String toString() {
		String cached = getCachedString();

		if (cached == null) {
			List<Cell> cells = getCellRange().getCells();

			for (Cell c : cells) {
				if (c == null) {
					cached = "#VALUE";
					setCachedString(cached);
					return cached;
				}
				else if (c.getContent() == null) {
					cached = "#VALUE";
					setCachedString(cached);
					return cached;	
				}
			}
			
			cached = "" + solve();
			setCachedString(cached);
		}

		return cached;
	}

	public static RangeFunction parse(Sheet sheet, String functionName, String args_exp) throws InvalidCellRange {
		Range range = Range.parse(args_exp);
		CompositeGamma cellRange = new CompositeGamma(sheet, range, false);
		RangeFunction rangeFn = null;

		switch (functionName) {
			case "AVG":
				rangeFn = new Avg(cellRange);
				break;
			case "PRD":
				rangeFn = new Prd(cellRange);
				break;
		}

		if (rangeFn != null) {
			sheet.registerObserver(range, rangeFn);
			rangeFn.setNumObservers(range.size());
		}
		
		return rangeFn;
	}
}