package calc.core;

import java.util.List;
import java.util.Arrays;

import calc.core.content.Content;
import calc.core.content.Literal;
import calc.textui.edit.InvalidCellRange;

/**
 * 
 * @author Group 06
 * @version 1.0
 */
public class Cell implements Gamma {
	/**
	 * The line of the cell.
	 */
	private int line;
	
	/**
	 * The collum of the cell.
	 */
	private int column;
	
	/**
	 * The expression of the cell.
	 */
	private String expression;

	/**
	 * The content of the cell.
	 */
	private Content content;
	
	/**
	 * Constructor creates a cell.
	 * @param the line.
	 * @param the column.
	 * @param the expression.
	 */	
	public Cell(int l, int c) {
		this.line = l;
		this.column = c;
		this.expression = "";
		this.content = null;
	}

	public Cell(int l, int c, String exp, Content content) {
		this.line = l;
		this.column = c;
		this.expression = exp;
		this.content = content;
	}
	
	/**
	 * Used to deep copy a Cell
	 */
	public Cell(int l, int c, Cell cell) {
		this.line = l;
		this.column = c;
		this.expression = cell.getExpression();
		this.content = cell.getContent();
	}

	public int getLine() {
		return this.line;
	}

	public int getColumn() {
		return this.column;
	}

	public String getExpression() {
		return this.expression;
	}

	public Content getContent() {
		return this.content;
	}

	public boolean isEmpty() {
		return this.expression.isEmpty();
	}

	public int solve() {
		return this.content.solve();
	}

	public String toString() {
		StringBuilder output = new StringBuilder(this.line + ";" + this.column + "|");

		if (!this.expression.isEmpty()) {
			output.append(this.content.toString());

			if (!(this.content instanceof Literal)) {
				output.append(this.expression);
			}
		}

		return output.toString();
	}

	public List<Cell> getCells() {
		return Arrays.asList(this);
	}
}