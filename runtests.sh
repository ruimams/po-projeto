#!/bin/bash

for x in tests/*.in; do
    if [ -e ${x%.in}.import ]; then
        java -cp pt.jar:calc-support.jar:. -Dimport=${x%.in}.import -Din=$x -Dout=${x%.in}.outhyp calc.textui.Calc;
    else
        java -cp pt.jar:calc-support.jar:. -Din=$x -Dout=${x%.in}.outhyp calc.textui.Calc ;
    fi
    
    base=${x##*/}
    diff -B -w tests/expected/${base%.in}.out ${x%.in}.outhyp > ${x%.in}.diff ;
    if [ -s ${x%.in}.diff ]; then
        echo "FAIL: $x" ;
        cat ${x%.in}.diff ;
        exit 0 ;
    else
        # echo -n "."
        rm -f ${x%.in}.diff ${x%.in}.outhyp ; 
    fi
done

rm -f *.dat

echo "Done."

