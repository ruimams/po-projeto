PROJ=calc
all:
	javac `find $(PROJ) pt -name "*.java"`
	jar -cf pt.jar `find pt -name "*.class"`
	jar -cfm $(PROJ).jar MANIFEST.MF pt.jar `find $(PROJ) -name "*.class"`
run:
	java -jar $(PROJ).jar
swing:
	java -Dui=swing -jar $(PROJ).jar
fast:
	javac `find $(PROJ) pt -name "*.java"`
entrega:
	jar cfv $(PROJ)-`date "+%Y%m%d%H%M%S"`.jar `find $(PROJ) -name "*.java"`
clean:
	rm -f `find $(PROJ) pt -name "*.class"`
